Synology Drive
=========

Install synology drive on CentOS base on the COPR repo by Emixampp

https://copr.fedorainfracloud.org/coprs/emixampp/synology-drive/


Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.synology_drive

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
